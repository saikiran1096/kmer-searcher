#include <queue>
#include <vector>
#include <map>
#include <array>

#include "trie.hpp"

Trie::Trie(){
  array4 t;
  t.fill(FAIL);
  g.push_back(t);
  match.push_back(false);
  f.push_back(FAIL);
}

void Trie::insert(std::string s){
  int state = 0;
  for(unsigned int i = 0; i < s.length(); i ++){
    int a = get_ind(s[i]);
    if(g[state][a] == FAIL){
      g[state][a] = g.size();
      array4 t;
      t.fill(FAIL);
      g.push_back(t);
      match.push_back(false);
      f.push_back(FAIL);
    }

    state = g[state][a];
      
  }
  match[state] = true;
  // kmer[state] = s;
}

int Trie::contains_match(std::string s){
  int state = 0;

  for(unsigned int i = 0; i < s.length(); i++){
    int a = get_ind(s[i]);
    if(a == -1){
      state = 0;
      continue;
    }
    while(g[state][a] == FAIL){
      state = f[state];
    }
    state = g[state][a];
    if(match[state]) {
      return state;
    }
  }

  return -1;
}

void Trie::compute_failures(){
  g.shrink_to_fit();
  f.shrink_to_fit();
  match.shrink_to_fit();
  std::queue<int> q;
  for(int i = 0; i < 4; i++){
    if(g[0][i] == FAIL){
      g[0][i] = 0;
    }else{
      int depth1_child = g[0][i];
      f[depth1_child] = 0;
      // std::cout << n->ind << " " << root->ind << std::endl;
      q.push(depth1_child);
    }
  }
    
  while(!q.empty()){
    int r = q.front();
    q.pop();

    for(int a = 0; a < 4; a++){
      int s = g[r][a];
      if(s == FAIL){
	continue;
      }
      q.push(s);
	
      int state = f[r];
      while(g[state][a] == FAIL){
	state = f[state];
      }
      f[s] = g[state][a];
    }
  }
}
