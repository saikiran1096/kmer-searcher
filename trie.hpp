#pragma once

#include <string>
#include <array>

using array4 = std::array<int, 4>;
const int FAIL = -1;

class Trie {
  inline int get_ind(char c){
    int ind = -1;
    switch(c){
    case 'A':
      ind = 0;
      break;
    case 'C':
      ind = 1;
      break;
    case 'G':
      ind = 2;
      break;
    case 'T':
      ind = 3;
      break;
    }

    return ind;
  }

public:
  std::vector<array4> g;
  std::vector<int> f;
  std::vector<bool> match;
  // std::map<int, std::string> kmer;

  void insert(std::string s);
  int contains_match(std::string s);
  void compute_failures();
  Trie();

};
