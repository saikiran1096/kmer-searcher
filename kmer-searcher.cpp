#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include "trie.hpp"
#include "blocking_queue.hpp"

using entry = std::array<std::string, 4>;
const int Q_SIZE = 2048;
  
void reader(std::ifstream* seqs, BlockingQueue<entry>* entries_q){
  while(seqs->peek() != EOF){
      std::string s;
      entry e;
      std::getline(*seqs, s);
      e[0] = s;
      std::getline(*seqs, s);
      e[1] = s;
      std::getline(*seqs, s);
      e[2] = s;
      std::getline(*seqs, s);
      e[3] = s;

      entries_q->push(e);
  }
}

void searcher(Trie* trie, BlockingQueue<entry>* entries_q, BlockingQueue<std::string>* outputs_q){
  while(true){
    entry e;
    int result = entries_q->pop(e);
    if(result){
      break;
    }

    int match = trie->contains_match(e[1]);

    if(match != -1){
      outputs_q->push(e[0]);
    }  

  }
}

void writer(std::ostream* out, BlockingQueue<std::string>* outputs_q){
  while(true){
    std::string s;
    int result = outputs_q->pop(s);
    if(result){
      break;
    }

    *out << s << std::endl;
  }
}

int main(int argc, char** argv){
  Trie trie;
  std::ifstream in(argv[1]);
  std::ifstream seqs(argv[2]);
  std::string output_file = argv[3];
  std::ostream* out;
  
  if(output_file == "-"){
    out = &std::cout;
  }
  else {
    std::ofstream of(output_file);
    out = &of;
  }

  int n_threads = atoi(argv[4]);
  
  std::string line;
  while(in.peek() != EOF){
    std::getline(in, line);
    trie.insert(line);
  }
  trie.compute_failures();

  BlockingQueue<entry>* entries_q = new BlockingQueue<entry>(Q_SIZE);
  BlockingQueue<std::string>* outputs_q = new BlockingQueue<std::string>(Q_SIZE);

  std::thread reader_thread(reader, &seqs, entries_q);
  std::thread writer_thread(writer, out, outputs_q);

  std::vector<std::thread*> searchers;

 for(int i = 0; i < n_threads; i++){
   searchers.push_back(new std::thread(searcher, &trie, entries_q, outputs_q));
 }

  
  reader_thread.join();
  entries_q->finish();

  for(int i = 0; i < n_threads; i++){
    searchers[i]->join();
    delete searchers[i];
  }
  
  outputs_q->finish();
  writer_thread.join();

  delete entries_q;
  delete outputs_q;
}
