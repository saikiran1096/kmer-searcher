#pragma once

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template<class T>
class BlockingQueue {
  std::queue<T> q;
  const size_t max_size;
  bool done_;
  
  std::condition_variable poppable;
  std::condition_variable pushable;
  std::mutex mtx;

public: 
  BlockingQueue(size_t max_size): max_size(max_size){
    done_ = false;
  }

  void finish(){
    done_ = true;
    poppable.notify_all();
  }

  void push(T item) {
    std::unique_lock<std::mutex> lck(mtx);
    while(q.size() == max_size){
      pushable.wait(lck);
    }

    q.push(item);
    poppable.notify_one();
  }

  int pop(T& result) {
    std::unique_lock<std::mutex> lck(mtx);
    while(q.size() == 0 && !done_){
      poppable.wait(lck);
    }

    if(q.size() == 0){
      return -1;
    }
  
    result = q.front();
    q.pop();

    pushable.notify_one();
    return 0;
  }

  size_t size(){
    return q.size();
  }
};

