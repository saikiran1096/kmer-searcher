.PHONY = all debug clean

CXX = g++
CC = g++
LDFLAGS = -pthread
CXXFLAGS = -Wall -pthread

OBJS = kmer-searcher.o trie.o

all: CXXFLAGS += -Ofast -march=native
all: LDFLAGS += -Ofast -march=native
all: kmer-searcher

debug: CXXFLAGS += -Og -g -pg
debug: LDFLAGS += -Og -g -pg
debug: kmer-searcher

kmer-searcher.o: blocking_queue.hpp trie.hpp
trie.o: trie.hpp

kmer-searcher: $(OBJS)

clean:
	rm kmer-searcher $(OBJS)
