**Compilation instructions**

`make`
    
**Usage instructions:**

kmer-searcher [kmers file] [sequences file] [output file] [number of mapping threads]
